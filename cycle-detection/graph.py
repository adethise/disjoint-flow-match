#!/usr/bin/env python3

from collections import defaultdict

class CycleDetected(Exception):
    pass

class DirectedAcyclicGraph:
    def __init__(self):
        self._links = defaultdict(set)

    def add_child(self, node, child):
        if child not in self._links[node]:
            self._links[node].add(child)
            if self.has_cycle_from(child):
                self._links[node].remove(child)
                raise CycleDetected

    def has_cycle_from(self, source):
        marked = set()
        closed = set()

        def visit(node):
            if node in marked:
                raise StopIteration
            else:
                marked.add(node)
                for child in self._links[node]:
                    if not child in closed:
                        visit(child)
                marked.remove(node)
                closed.add(node)

        try:
            visit(source)
            return False
        except StopIteration:
            return True
