#!/usr/bin/env python3

import os
import subprocess
import random

from collections import defaultdict
from copy import deepcopy

from graph import DirectedAcyclicGraph, CycleDetected


EXEC = os.path.join(os.path.dirname(__file__), 'forwarding_path.exe')


#############################
# Network entities template #
#############################

class NetworkEntity:
    """
    Network operator entities.
    Network entities are uniquely identified by their entitynumber.
    """
    def __init__(self, entitynum):
        assert isinstance(entitynum, int)
        if network_entities.get(entitynum):
            raise ValueError('Entity number %d already in use' % entitynum)
        else:
            network_entities[entitynum] = self
            self.entitynum = entitynum

    def __eq__(self, other):
        return (isinstance(other, NetworkEntity)
                and self.entitynum == other.entitynum)

    def __hash__(self):
        return hash(self.entitynum)

network_entities = {}


###############################
# Global SDX information base #
###############################

class SDXRepo:
    def __init__(self):
        self.sdxes = set()
        self.knowledge = defaultdict(lambda: None)

    def register(self, sdx):
        self.sdxes.add(sdx)

    def notify_appartenance(self, sdx, member):
        assert sdx in self.sdxes
        self.knowledge[member] = sdx

    def get_appartenance(self, ases):
        try:
            return [self.knowledge[member] for member in ases]
        except TypeError: # If ases is not iterable but a single AS
            return self.knowledge[ases]

global_sdx_information = SDXRepo()


####################
# SDX entity class #
####################

class SDX(NetworkEntity):
    def __init__(self, sdxnum):
        super().__init__(sdxnum)

        self.members = set()
        global_sdx_information.register(self)

        self.tokens_given = {} # {token: member}
        self.tokens_received = defaultdict(list) # {member: (SDX, token)}

    def __repr__(self):
        return 'SDX-%d' % self.sdxnum

    @property
    def sdxnum(self):
        return self.entitynum

    ##############
    # Public API #
    ##############

    def member_join(self, member):
        self.members.add(member)
        global_sdx_information.notify_appartenance(self, member)


    def member_rule_request(self, requestor, rule, aspath):
        """
        Verify the validity of the rule and raise CycleDetected if that rule
        would create a forwarding loop.
        Returns a set of (SDX, member) for each deflection in the AS path if
        the rule is valid.

        Params:
            member  the SDX member who requested that rule installation
            rule    the rule to be installed
            aspath  the AS path toward which traffic will be deflected

        Exceptions:
            CycleDetected      if the rule can not be installed
        """
        print('Member %s requested the installation of a new rule at %s' %
                (requestor, self))

        # get SDXes in ASpath
        sdxpath = self._get_sdxes_in_path(aspath)

        if len(sdxpath) == 0:
            print('No SDX crossed in the AS path, installation authorized')
            return 0, set()
        else:
            print('SDXes in the path: %s' % sdxpath)

            graph = DirectedAcyclicGraph() # Graph of deflections
            # Will throw CycleDetected if a cycle is created in add_child

            token = random.randrange(2**64)
            self.tokens_given[token] = requestor

            try:
                ## Find the deflections from the AS path
                aspath_deflections = set()
                to_visit = defaultdict(set)

                for sdx, members in sdxpath:
                    # since we are deflecting towards them, add it to graph
                    graph.add_child(self, sdx)

                    # since we are deflecting towards them, add it to return
                    for member in members:
                        aspath_deflections.add((sdx, member))


                    # here members should be PK encoded
                    # members = [encode(m, pk[sdx]) for m in members]
                    nexthops = self.make_nexthop_request(rule, sdx, members,
                            token)

                    # for each deflection we have to follow the flow
                    for next_sdx, next_as in nexthops:
                        to_visit[next_sdx].add(next_as)
                        graph.add_child(sdx, next_sdx)

                ## Follow the deflections to detect cycles
                # If there is no loop we will exit normally, otherwise the DAG
                # will detect it and throw an exception
                while to_visit: # while we have sdxes to visit
                    sdx, members = to_visit.popitem() # choose one randomly
                    # todo: parallelize requests
                    nexthops = self.make_nexthop_request(rule, sdx, members)

                    # for each deflection we have to follow the flow
                    for next_sdx, next_as in nexthops:
                        to_visit[next_sdx].add(next_as)
                        graph.add_child(sdx, next_sdx)

                # No exception, no loop detected
                print('Installation authorized, next hops: %s' %
                        aspath_deflections)

                # Installation accepted, issue revokations
                try:
                    for sdx, revoked in self.tokens_received.pop(requestor):
                        sdx.revoke_token(revoked)
                except KeyError:
                    pass # No token affect this member

                return token, aspath_deflections

            except CycleDetected:
                print('Loop detected, installation refused')
                raise

    def make_nexthop_request(self, rule, server, members, token):
        """
        Run SMPC to verify the possible deflection destination for this rule
        against the deflections at the server SDX.
        Return a list of (sdx, member).

        Params:
            rule        the rule defining the flow we are looking
            server      the remote SDX, acting as a server in the SMPC
            members     the members to query at the server
        """
        # Here we should use pre-created ABY sessions
        # (or create one if it hasn't been pre-established)

        print('%s requesting next hop information to %s' % (self, server))

        # Ask the server to receive our request for its members
        # todo: add public bits
        num_rules = server.receive_nexthop_request(self, members, rule.length,
                token)

        if num_rules == 0:
            return [] # do not exec SMPC with no rule (short-circuited)

        client = subprocess.Popen(
                [EXEC,
                    '-r', '1',
                    '-l', str(rule.length),
                    '-n', str(num_rules)],
                stdin = subprocess.PIPE,
                stdout = subprocess.PIPE,
                universal_newlines = True
        )
        print(rule, file = client.stdin)
        client.stdin.flush()

        try:
            client.wait(timeout = 20) # Should be replaced by reading output
        except subprocess.TimeoutExpired:
            print('Timeout expired during SMPC computation')
            raise ValueError('Could not verify the rule')

        output = client.stdout.read()

        # convert the output to their entitynumbers
        deflections = [map(int, line.split(':')) for line in output.split()]
        # convert entitynumbers to entity objects
        deflections = [map(network_entities.get, nums) for nums in deflections]

        return deflections

    def receive_nexthop_request(self, requestor, members, length, token):
        rules = []
        for m in members:
            # here members should be PK decoded
            # m = decode(m, self.sk)
            assert m in self.members
            self.tokens_received[m].append((requestor, token))
            rules += m.rules

        deflecting_rules = [(rule, deflection) for rule, deflection in rules
                if deflection]

        num_rules = len(deflecting_rules)

        if num_rules == 0:
            return 0 # do not exec SMPC with no rule

        process = subprocess.Popen(
                [EXEC,
                    '-r', '0',
                    '-l', str(length),
                    '-n', str(num_rules)],
                stdin = subprocess.PIPE,
                stdout = subprocess.DEVNULL,
                universal_newlines = True
        )
        for rule, (sdx, member) in deflecting_rules:
            print(rule, file = process.stdin)
            print(sdx.sdxnum, file = process.stdin)
            print(member.asnum, file = process.stdin)
        process.stdin.flush()

        return num_rules

    def revoke_token(self, token):
        try:
            member = self.tokens_given.pop(token)
            member.revoke_rule(token)
            print('Member %s was forced to revoke a rule with token %d' %
                    (member, token))
        except KeyError:
            pass # another SDX probably already revoked this rule

    ########################
    # Support subfunctions #
    ########################

    @staticmethod
    def _get_sdxes_in_path(aspath):
        sdxpath = global_sdx_information.get_appartenance(aspath)

        saved_start = -1
        saved_sdx = None

        output = []

        for i, sdx in enumerate(sdxpath + [None]):
            if sdx != saved_sdx:
                if saved_sdx and (i - saved_start) > 1:
                    output.append((saved_sdx, aspath[saved_start:i]))
                saved_start = i
                saved_sdx = sdx

        return output


###################
# AS entity class #
###################

class AS(NetworkEntity):
    def __init__(self, asnum, sdx = None):
        super().__init__(asnum)

        self.sdx = sdx # todo: support belonging to multiple SDXes
        self.rules = []
        self.tokens = {}

        if self.sdx: # if we belong in a SDX, register as such
            self.sdx.member_join(self)

    def __repr__(self):
        return 'AS-%d' % self.asnum

    @property
    def asnum(self):
        return self.entitynum

    ##############
    # Public API #
    ##############

    def install_rule(self, rule, aspath):
        # todo: replace aspath with Route and verify that it was annouced
        try:
            token, nexthops = self.sdx.member_rule_request(self, rule, aspath)

            if nexthops:
                self.tokens[token] = rule
            for nexthop in nexthops:
                self.rules.append((rule, nexthop))

        except CycleDetected:
            pass

        print('-----')

    def revoke_rule(self, token):
        revoked_rule = self.tokens.pop(token)
        self.rules = [(rule, nexthop) for rule, nexthop in self.rules
                if rule != revoked_rule]
