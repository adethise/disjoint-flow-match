#!/usr/bin/env python3

# Import network entity types
import entities
import routing

# Create network entities
SDX1 = entities.SDX(65001)
SDX2 = entities.SDX(65002)
A = entities.AS(1, SDX2)
B = entities.AS(2, SDX2)
M = entities.AS(3, SDX1)
X = entities.AS(4)
Y = entities.AS(5)
Z = entities.AS(6)

C = entities.AS(10, SDX2)
D = entities.AS(11, SDX2)
N = entities.AS(12, SDX1)

SDX3 = entities.SDX(65003)
P = entities.AS(20, SDX3)
Q = entities.AS(21, SDX3)

# Create rules
rule1 = routing.Rule(
        'FF000000000000000000000000',
        'FF000000000000000000000000')
rule2 = routing.Rule(
        '88000000000000000000000000',
        'FF000000000000000000000000')
rule3 = routing.Rule(
        '000000000000AAAA0000000000',
        '000000000000FFFF0000000000')

# Install rules
A.install_rule(rule1, [Y, X]) # SDX path = []
M.install_rule(rule1, [Y, X, B, A]) # SDX path = [2]
A.install_rule(rule1, [Z, B, A, Y, D, C, X, N, M]) # SDX path = [1, 2, 2]
M.install_rule(rule1, [Y, X, Q, P, B, A]) # SDX path = [2, 3]
B.install_rule(rule1, [Z, N, M, X]) # SDX path = [1]
Q.install_rule(rule1, [Z, B, A, N, M, Y]) # SDX path = [1, 2]
Q.install_rule(rule2, [Z, B, A, N, M, Y]) # SDX path = [1, 2]
N.install_rule(rule2, [Z, B, A]) # SDX path = [2]
