#!/usr/bin/env python3


class Prefix:
    def __init__(self, s):
        ipstr, prefixlen = s.split('/')
        ipstr = ipstr.split('.')
        assert len(ipstr) == 4

        self.ip = 0
        for f in ipstr:
            self.ip *= 255
            self.ip += int(f)

        mask = 0xFFFFFFFF << (32 - int(prefixlen)) & 0xFFFFFFFF

    def __eq__(self, other):
        return self.ip == other.ip and self.mask == other.mask

    def __hash__(self):
        return hash((self.ip, self.mask))


class Route:
    def __init__(self, prefix, aspath):
        if isinstance(prefix, Prefix):
            self.prefix = prefix
        else:
            self.prefix = Prefix(prefix)

        self.aspath = tuple(aspath)

    def __eq__(self, other):
        return self.prefix == other.prefix and self.aspath == other.aspath

    def __hash__(self):
        return hash((self.prefix, self.aspath))


class Rule:
    def __init__(self, ptrn, mask, length = -1, clean = True):
        if length < 0:
            self.length = len(ptrn) // 2
        else:
            self.length = length

        self.ptrn = int(ptrn[0:2*self.length], 16)
        self.mask = int(mask[0:2*self.length], 16)

        if clean:
            # Ignore pattern bits that are not watch by the mask
            self.ptrn &= self.mask

    def __str__(self):
        form = '%.' + str(2 * self.length) + 'x'
        return form % self.ptrn + '\n' + form % self.mask

    def __eq__(self, other):
        return (
                self.ptrn == other.ptrn and
                self.mask == other.mask and
                self.length == other.length
        )

    def __hash__(self):
        return hash((self.ptrn, self.mask, self.length))

    def is_disjoint(self, other):
        '''
        Returns true is the rules self and other match disjoint flows.
        '''
        return ((self.ptrn ^ other.ptrn) & self.mask & other.mask) != 0
