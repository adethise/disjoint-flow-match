#!/usr/bin/env python3

'''
Run the distinct match algorithm in outsourced mode.
This script will run locally the server and client instances of the program and
send them XOR-encrypted shares of the inputs.

author: Arnaud Dethise <arnaud.dethise@kaust.edu.sa>
date: 2017-03-27
'''

import argparse
import subprocess
import random
import sys


def h2s(l, v):
    '''
    Returns the hexadecimal string of length l representing the value v.
    '''
    return ('%0' + str(l) + 'x') % v


class Rule:
    '''
    Class representing a match rule, with pattern and mask of bytelem bytes.
    '''
    def __init__(self, bytelen, str_ptrn, str_mask):
        self.bytelen = bytelen
        self.ptrn = int(str_ptrn[0:2*bytelen], 16)
        self.mask = int(str_mask[0:2*bytelen], 16)

        maxval = 256 ** bytelen
        self.pkey = random.randrange(maxval)
        self.mkey = random.randrange(maxval)

    def share(self, target):
        '''
        Returns the shares for target.
        The target can be 'server' or 'client', or 'nonencrypt' for debugging.
        '''
        l = 2 * self.bytelen
        if target.lower() == 'nonencrypt':
            return h2s(l, self.ptrn), h2s(l, self.mask)
        elif target.lower() == 'server':
            return h2s(l, self.pkey), h2s(l, self.mkey)
        elif target.lower() == 'client':
            return h2s(l, self.ptrn ^ self.pkey), h2s(l, self.mask ^ self.mkey)
        else:
            raise ValueError('Wrong target')


def parse_rules(filename, nrules, bytelen):
    '''
    Parse <nrules> rules of length <bytelen> from <filename>
    and returns them as an array of rules.
    '''
    with open(filename, 'r') as f:
        _ret = []
        for i in range(nrules):
            _ret.append(Rule(bytelen, f.readline(), f.readline()))
    return _ret


def run(exe, mode, bytelen, rules):
    '''
    Run the distinct match software in outsourced mode and prints the server and
    client outputs.

    exe     -- path to the distinct_match.exe executable file
    mode    -- 0 for MOX, 1 for CLEAR, 2 for COLLAPSED
    bytelen -- length of the match rules, in bytes
    rules   -- list of the match rules, with rules[0] being the client rule
    '''

    server = run_for('server', exe,
            str(mode), str(bytelen), str(len(rules)-1), rules)
    client = run_for('client', exe,
            str(mode), str(bytelen), str(len(rules)-1), rules)

    try:
        server.wait(timeout = 20)
        client.wait(timeout = 1) # Client should already be terminated
    except subprocess.TimeoutExpired:
        print('Timeout expired')

    print('#####################')
    print('### Server output ###')
    print('#####################')
    print(server.stdout.read())
    print('#####################')
    print('### Client output ###')
    print('#####################')
    print(client.stdout.read())


def run_for(target, exe, mode, length, nrules, rules):
    '''
    Low-level version of run. Target can be 'server' or 'client'
    '''

    role = '0' if target == 'server' else '1'
    process = subprocess.Popen(
            [exe, '-r', role, '-l', length, '-n', nrules, '-m', mode, '-x'],
            stdin = subprocess.PIPE,
            stdout = subprocess.PIPE,
            universal_newlines = True
    )

    for p, m in [r.share(target) for r in rules]:
        process.stdin.write(p + '\n')
        process.stdin.write(m + '\n')

    process.stdin.close()

    return process


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description = 'Run the distinct match algorithm program in ' +
            'outsourced mode')
    parser.add_argument('execfile',
            help = 'path to the distinct_match.exe file')
    parser.add_argument('-l', dest = 'bytelen', type = int,
            help = 'length of the rules')
    parser.add_argument('-n', dest = 'nrules', type = int,
            help = 'number of server rules')
    parser.add_argument('-m', dest = 'mode', type = int,
            help = 'run mode (0 = MXOR, 1 = CLEAR, 2 = COLLAPSED)')
    parser.add_argument('client_file')
    parser.add_argument('server_file')

    args = parser.parse_args()


    rules = []
    rules += parse_rules(args.client_file, 1, args.bytelen)
    rules += parse_rules(args.server_file, args.nrules, args.bytelen)

    run(args.execfile, args.mode, args.bytelen, rules)
