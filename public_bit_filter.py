#!/usr/bin/env python3

'''
Filter streams of match rules to remove those that are disjoint from the client
rule based on public bits alone.

Recommended use :
    cat server_rules.txt | python3 filter.py public.txt | ./disjoint_match.exe
'''

import argparse
import sys

class Rule:
    def __init__(self, length, ptrn, mask):
        self.length = length
        self.ptrn_raw = ptrn
        self.mask_raw = mask

        self.ptrn = int(ptrn[0:2*length], 16)
        self.mask = int(mask[0:2*length], 16)

    def is_disjoint(self, other):
        '''
        Returns true is the rules self and other match disjoint flows.
        '''
        return ((self.ptrn ^ other.ptrn) & self.mask & other.mask) != 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Filter match rules')
    parser.add_argument('-l', dest = 'length', type = int,
            help = 'length of the public bits rule')
    parser.add_argument('public_bits', help = 'File of the public rule')

    args = parser.parse_args()

    with open(args.public_bits, 'r') as f:
        public = Rule(args.length, f.readline(), f.readline())

    while True:
        first = sys.stdin.readline()[0:-1] # Strip \n
        second = sys.stdin.readline()[0:-1]
        if not first or not second:
            break

        r = Rule(args.length, first, second)
        if not public.is_disjoint(r): # only output if not distinct
            print(r.ptrn_raw)
            print(r.mask_raw)
