#!/usr/bin/env bash

sn=distinct_match
exdir=ABY/src/examples/distinct_match
exe=./ABY/bin/distinct_match.exe
if [ ! -e $exe ] ; then
	cd ABY/
	make
	cd -
fi


tmux new-session -s $sn -d

tmux split-window -t "$sn:0"
tmux send-keys -t "$sn:0.0" "$exe -r 0 -l 18 -n 256"
tmux send-keys -t "$sn:0.0" " < $exdir/example-input-multiple-server-l18-r256"
tmux send-keys -t "$sn:0.1" "$exe -r 1 -l 18 -n 256"
tmux send-keys -t "$sn:0.1" " < $exdir/example-input-l18-2"
tmux -2 attach-session -t "$sn"
