#include "../../../abycore/circuit/booleancircuits.h"
#include "../../../abycore/circuit/circuit.h"

/**
 * \brief	This function is used to find all bits were the matched
 * 		patterns are disjoint
 * \param	circ		Boolean circuit to build upon
 * \param	in1		Shared object of the first match pattern
 * \param	mask1		Shared object of the first bitmask
 * \param	in2		Shared object of the second match pattern
 * \param	mask2		Shared object of the second bitmask
 */
share* PutMaskedXORGate(
		BooleanCircuit *circ,
		share* in1, share* mask1,
		share* in2, share* mask2);

/**
 * \brief	Return a share with a single bit of value 1 if any bit in the
 * 		input is 1, 0 otherwise
 * \param	circ		Boolean circuit to build upon
 * \param	input		Input share
 */
share* PutORTree(BooleanCircuit *circ, share* input);

/**
 * \brief	Return a share with a single bit of value 1 if every bit in the
 * 		input is 1, 0 otherwise
 * \param	circ		Boolean circuit to build upon
 * \param	input		Input share
 */
share* PutANDTree(BooleanCircuit *circ, share *input);
