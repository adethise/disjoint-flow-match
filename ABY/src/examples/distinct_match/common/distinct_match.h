/**
 \file 		distinct_match.h
 \author 	Arnaud Dethise <arnaud.dethise@kaust.edu.sa>
 \copyright	ABY - A Framework for Efficient Mixed-protocol Secure Two-party Computation
			Copyright (C) 2015 Engineering Cryptographic Protocols Group, TU Darmstadt
			This program is free software: you can redistribute it and/or modify
			it under the terms of the GNU Affero General Public License as published
			by the Free Software Foundation, either version 3 of the License, or
			(at your option) any later version.
			This program is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
			GNU Affero General Public License for more details.
			You should have received a copy of the GNU Affero General Public License
			along with this program. If not, see <http://www.gnu.org/licenses/>.
 \brief		Implementation of the distinct match detector using ABY Framework.
 */

#ifndef __DISTINCT_MATCH_H_
#define __DISTINCT_MATCH_H_

#include "../../../abycore/circuit/booleancircuits.h"
#include "../../../abycore/circuit/circuit.h"
#include "../../../abycore/aby/abyparty.h"

#include "gates.h"

#include <cassert>

/**
 * \enum	e_mode
 * \brief	Enumeration for the modes SINGLE and MULTIPLE
 */
enum e_mode {
	MXOR, CLEAR, COLLAPSED
};

share* BuildMXORCircuit(BooleanCircuit *circ,
		share *in1, share *mask1,
		share *in2, share *mask2);

share* BuildCLEARCircuit(BooleanCircuit *circ,
		share *in1, share *mask1,
		share *in2, share *mask2);

share* BuildCOLLAPSEDCircuit(BooleanCircuit *circ,
		share *in1, share *mask1,
		share *in2, share *mask2);

void add_input_gates(share **s_p1, share **s_m1, share **s_p2, share **s_m2,
		e_role role, BooleanCircuit *circ, uint8_t *p, uint8_t *m,
		uint32_t nvals, uint32_t bitlen, bool outsourced,
		bool optimized_inputs = false);

/**
 * \brief	This function is used for running a testing environment for
 		solving the distinct flow match detection problem.
 * \param	role		Role of the executer in the SMPC computation
 * \param	address		IP Address of the SMPC computation server
 * \param	seclvl		Security level
 * \param	num_rules_s	Number of rules held by the server
 * \param	bytelen		Byte length of the match rule being tested
 * \param	nthreads	Number of threads used for the computation
 * \param	mt_alg		Multiplication triples generation algorithm
 */
int32_t test_distinct_match_circuit(
		e_role role, e_mode mode, uint32_t bytelen, bool outsourced,
		uint32_t num_rules_s, e_role out_party, e_sharing sharing,
		seclvl seclvl, char* address, uint16_t port,
		uint32_t nthreads, e_mt_gen_alg mt_alg);


/**
 * \brief	Parse a line of bytelen bytes into the res buffer
 * \param	s	String containing the hex values (must be at least 2 *
 * 			bytelen characters long)
 * \param	res	Buffer where the value is to be stored
 * \param	bytelen	Number of bytes to parse from the string
 */
void parse_hexline(string s, uint8_t* res, uint32_t bytelen);

void print_hexline(string infostring, uint8_t *data, uint32_t length);

#endif /* __DISTINCT_MATCH_H_ */
