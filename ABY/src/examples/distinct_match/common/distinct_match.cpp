/**
 \file 		distinct_match.cpp
 \author 	Arnaud Dethise <arnaud.dethise@kaust.edu.sa>
 \copyright	ABY - A Framework for Efficient Mixed-protocol Secure Two-party Computation
			Copyright (C) 2015 Engineering Cryptographic Protocols Group, TU Darmstadt
			This program is free software: you can redistribute it and/or modify
			it under the terms of the GNU Affero General Public License as published
			by the Free Software Foundation, either version 3 of the License, or
			(at your option) any later version.
			This program is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
			GNU Affero General Public License for more details.
			You should have received a copy of the GNU Affero General Public License
			along with this program. If not, see <http://www.gnu.org/licenses/>.
 \brief		Implementation of the distinct match detector using ABY Framework.
 */

#include "distinct_match.h"

#define DISTINCT_MATCH_OUT 1
#define DISTINCT_MATCH_DBG_OUT 1
#define DISTINCT_MATCH_DBG_OUT_COARSE 0

share* BuildMXORCircuit(BooleanCircuit *circ,
		share *in1, share *mask1,
		share *in2, share *mask2)
{
	return PutMaskedXORGate(circ, in1, mask1, in2, mask2);
}

share* BuildCLEARCircuit(BooleanCircuit *circ,
		share *in1, share *mask1,
		share *in2, share *mask2)
{
	share *shr_compare, *shr_result;

	shr_compare = BuildMXORCircuit(circ, in1, mask1, in2, mask2);
	shr_result = circ->PutINVGate(
			PutANDTree(circ, circ->PutINVGate(shr_compare)));

	return shr_result;
}

share* BuildCOLLAPSEDCircuit(BooleanCircuit *circ,
		share *in1, share *mask1,
		share *in2, share *mask2)
{
	share *results, *wires, *collapsed;

	results = BuildCLEARCircuit(circ, in1, mask1, in2, mask2);
	wires = circ->PutSplitterGate(results);
	collapsed = PutANDTree(circ, wires);

	return collapsed;
}

/**
 * Parse a line of bytelen bytes into a uint8_t array.
 * res must be an array of uint8_t large enough to store bytelen values.
 */
void parse_hexline(string s, uint8_t* res, uint32_t bytelen)
{
	assert(s.length() >= 2 * bytelen);

	char *cs = (char*) s.c_str();

	for (int i = 0 ; i < bytelen ; i++) {
		sscanf(cs+2*i, "%2hhx", res+i);
	}
}

void print_hexline(string infostring, uint8_t *data, uint32_t length)
{
	cout << infostring;
	cout << hex;
	for (uint32_t i = 0 ; i < length ; i++) {
		cout << setfill('0') << setw(2) << (int) data[i] << " ";
	}
	cout << endl << dec;
}

void add_input_gates(share **s_p1, share **s_m1, share **s_p2, share **s_m2,
		e_role role, BooleanCircuit *circ, uint8_t *p, uint8_t *m,
		uint32_t nvals, uint32_t bitlen, bool outsourced,
		bool optimized_inputs)
{
	assert(bitlen % 8 == 0);

	// If role == SERVER, p and m contain nvals values
	// If role == CLIENT, p and m contain one value
	// If outsourced, p and m contain nvals+1 values, the first one being
	// the client's rule
	share *s_p2_uniq, *s_m2_uniq;

	if (outsourced) {
		*s_p1 = circ->PutSharedSIMDINGate(nvals, p + bitlen/8, bitlen);
		*s_m1 = circ->PutSharedSIMDINGate(nvals, m + bitlen/8, bitlen);
		s_p2_uniq = circ->PutSharedINGate(p, bitlen);
		s_m2_uniq = circ->PutSharedINGate(m, bitlen);
	} else if (optimized_inputs) {
		// This branch will used shared inputs with one share being 0
		// to avoid an unnecessary communication round
		uint8_t* null =
			(uint8_t*) calloc(nvals, sizeof(uint8_t) * bitlen / 8);
		if (role == SERVER) {
			*s_p1 = circ->PutSharedSIMDINGate(nvals, p, bitlen);
			*s_m1 = circ->PutSharedSIMDINGate(nvals, m, bitlen);
			s_p2_uniq = circ->PutSharedINGate(null, bitlen);
			s_m2_uniq = circ->PutSharedINGate(null, bitlen);
		} else {
			*s_p1 = circ->PutSharedSIMDINGate(nvals, null, bitlen);
			*s_m1 = circ->PutSharedSIMDINGate(nvals, null, bitlen);
			s_p2_uniq = circ->PutSharedINGate(p, bitlen);
			s_m2_uniq = circ->PutSharedINGate(m, bitlen);
		}
	} else {
		if (role == SERVER) {
			*s_p1 = circ->PutSIMDINGate(nvals, p, bitlen, SERVER);
			*s_m1 = circ->PutSIMDINGate(nvals, m, bitlen, SERVER);
			s_p2_uniq = circ->PutDummyINGate(bitlen);
			s_m2_uniq = circ->PutDummyINGate(bitlen);
		} else {
			*s_p1 = circ->PutDummySIMDINGate(nvals, bitlen);
			*s_m1 = circ->PutDummySIMDINGate(nvals, bitlen);
			s_p2_uniq = circ->PutINGate(p, bitlen, CLIENT);
			s_m2_uniq = circ->PutINGate(m, bitlen, CLIENT);
		}
	}

	*s_p2 = circ->PutRepeaterGate(nvals, s_p2_uniq);
	*s_m2 = circ->PutRepeaterGate(nvals, s_m2_uniq);
}

/**
 * Run a test for the circuit reading from standard input.
 * The inputs must be a hex-encoded bit string with length at least bytelen.
 */
int32_t test_distinct_match_circuit(
		e_role role, e_mode mode, uint32_t bytelen, bool outsourced,
		uint32_t num_rules_s, e_role out_party, e_sharing sharing,
		seclvl seclvl, char* address, uint16_t port,
		uint32_t nthreads, e_mt_gen_alg mt_alg)
{

	uint32_t bitlen = bytelen * 8;
	uint32_t maxgates = 15 * (bitlen + num_rules_s);


	// Read input from cin
	// n is the number of rules to read
	// p, m are the memory areas containing the rules
	// ptrns, masks are the arrays containing pointers to the rules
	uint32_t n;
	if (outsourced) {
		n = num_rules_s + 1;
	} else {
		n = (role == SERVER) ? num_rules_s : 1;
	}

	uint32_t nvals = num_rules_s;
#if DISTINCT_MATCH_DBG_OUT
	cout << ">> Reading inputs from stdin" << endl;
	cout << "Expecting " << n << " rules" << endl;
#endif

	uint8_t *p, *m;
	uint8_t *ptrns[n], *masks[n];
	p = (uint8_t*) calloc(n, bytelen * sizeof(uint8_t));
	m = (uint8_t*) calloc(n, bytelen * sizeof(uint8_t));

	string input_line;
	for (uint32_t i = 0 ; i < n ; i++) {
		ptrns[i] = p + i * bytelen;
		getline(cin, input_line);
		parse_hexline(input_line, ptrns[i], bytelen);

		masks[i] = m + i * bytelen;
		getline(cin, input_line);
		parse_hexline(input_line, masks[i], bytelen);
	}

#if DISTINCT_MATCH_DBG_OUT_COARSE
	for (uint32_t i = 0 ; i < n ; i++) {
		print_hexline("Pattern: ", ptrns[i], bytelen);
		print_hexline("Bitmask: ", masks[i], bytelen);
	}
#endif

	// ABY setup
#if DISTINCT_MATCH_DBG_OUT
	cout << ">> Setting up the ABY party" << endl;
#endif
	ABYParty* party = new ABYParty(role, address, port,
			seclvl, bitlen, nthreads,
			mt_alg, maxgates);

#if DISTINCT_MATCH_DBG_OUT
	cout << ">> Building the circuit" << endl;
#endif
	vector<Sharing*>& sharings = party->GetSharings();
	BooleanCircuit* circ =
		(BooleanCircuit*) sharings[sharing]->GetCircuitBuildRoutine();


	// Shares containing the problem information
	// arrays p1 p2 m1 m2 contain the patterns and matches of each party
	// arrays compare and result contain intermediate values
	// array out contains the reconstructed values
	share *s_p1, *s_m1, *s_p2, *s_m2;
	share *s_result, *s_out;

	// Put actual gates for ourself, dummy gates for the other actor
	if (sharing == S_BOOL) {
		add_input_gates(&s_p1, &s_m1, &s_p2, &s_m2,
				role, circ, p, m, nvals, bitlen, outsourced,
				true);
	} else {
		add_input_gates(&s_p1, &s_m1, &s_p2, &s_m2,
				role, circ, p, m, nvals, bitlen, outsourced);
	}

	switch (mode) {
	case MXOR:
		s_result = BuildMXORCircuit(circ, s_p1, s_m1, s_p2, s_m2);
		break;
	case CLEAR:
		s_result = BuildCLEARCircuit(circ, s_p1, s_m1, s_p2, s_m2);
		break;
	case COLLAPSED:
		s_result = BuildCOLLAPSEDCircuit(circ, s_p1, s_m1, s_p2, s_m2);
		break;
	default:
		cerr << "Invalid mode" << endl;
			return 1;
	}

	s_out = circ->PutOUTGate(s_result, out_party);



#if DISTINCT_MATCH_DBG_OUT
	cout << ">> Built the circuit" << endl;
	cout << "Simulteous comparison of " << nvals << " rules" << endl;
#endif

	party->ExecCircuit();

#if DISTINCT_MATCH_DBG_OUT
	cout << ">> Computation complete, reading output" << endl;
#endif

	if (out_party == ALL || out_party == role) {
		// Read the output
		uint32_t out_bitlen, out_nvals;
		uint8_t	*out_vals;
		out_bitlen = s_out->get_bitlength();
		out_nvals = s_out->get_nvals_on_wire(0);
		out_vals = s_out->get_clear_value_ptr();

		// Verify that the bit length and nvals are as expected
		if (mode == MXOR)
			assert(out_bitlen == bitlen); // #result = #rules
		else //if (mode == CLEAR || mode == COLLAPSED)
			assert(out_bitlen == 1); // Collapsed in a single bit

		if (mode == MXOR || mode == CLEAR)
			assert(out_nvals == nvals); // #result = #rules
		else //if (mode == COLLAPSED)
			assert(out_nvals == 1); // Single, collapsed result

#if DISTINCT_MATCH_DBG_OUT
		cout << "=PARAMS=============" << endl;
		cout << "Bitlen: " << out_bitlen << endl;
		cout << "Nvals: " << out_nvals << endl;
		cout << "=OUTPUT=============" << endl;
#endif
#if DISTINCT_MATCH_OUT
		if (mode == MXOR) {
			cout << hex;
			for (uint32_t i = 0 ; i < out_nvals ; i++) {
				for (uint32_t j = 0 ; j < bytelen ; j++) {
					cout << setfill('0') << setw(2);
					cout << +out_vals[i * bytelen + j];
				}
				cout << endl;
			}
			cout << dec;
		} else { // if (mode == CLEAR || mode == COLLAPSED) {
			for (uint32_t i = 0 ; i < out_nvals ; i++) {
				cout << +out_vals[i];
			}
			cout << endl;
		}
#endif
	}

#if DISTINCT_MATCH_DBG_OUT
	cout << ">> Cleaning up" << endl;
#endif

	free(p);
	free(m);
	delete party;

	return 0;
}
