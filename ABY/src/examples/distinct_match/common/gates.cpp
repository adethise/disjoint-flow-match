#include "gates.h"

/**
 * Find all bits were the inputs differ and the mask of both is set to 1
 * In other words, returns the resulting share of the function
 * `mask1 & mask2 & (in1 ^ in2)`
 */
share* PutMaskedXORGate(
		BooleanCircuit *circ,
		share* in1, share* mask1,
		share* in2, share* mask2)
{
	share* bothcare  = circ->PutANDGate(mask1, mask2);
	share* different = circ->PutXORGate(in1, in2);
	share* distinct  = circ->PutANDGate(bothcare, different);

	return distinct;
}

/**
 * Returns a single bit corresponding to the logical OR of every wire in the
 * input share.
 *
 * In other words, return 1 iff any bit in the input is 1.
 */
share* PutORTree(BooleanCircuit *circ, share* input)
{
	// ABY primitives use INV and AND to build a OR gate anyway
	return circ->PutINVGate(PutANDTree(circ, circ->PutINVGate(input)));
}

/**
 * Returns a single bit corresponding to the logical AND of every wire in the
 * input share.
 *
 * In other words, return 1 iff every bit in the input is 1.
 */
share* PutANDTree(BooleanCircuit *circ, share *input)
{
	vector<uint32_t> wires = input->get_wires();

	while (wires.size() > 1) {
		uint32_t h = wires.size() / 2;
		vector<uint32_t> l (wires.begin(), wires.begin() + h);
		vector<uint32_t> r (wires.begin() + h, wires.begin() + 2 * h);
		vector<uint32_t> next = circ->PutANDGate(l, r);

		if (wires.size() % 2 != 0)
			next.push_back(wires[wires.size() - 1]);

		wires = next;
	}

	return new boolshare(wires, circ);
}
