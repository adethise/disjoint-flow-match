/**
 \file 		distinct_match.cpp
 \author	Arnaud Dethise <arnaud.dethise@kaust.edu.sa>
 \copyright	ABY - A Framework for Efficient Mixed-protocol Secure Two-party Computation
			Copyright (C) 2015 Engineering Cryptographic Protocols Group, TU Darmstadt
			This program is free software: you can redistribute it and/or modify
			it under the terms of the GNU Affero General Public License as published
			by the Free Software Foundation, either version 3 of the License, or
			(at your option) any later version.
			This program is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
			GNU Affero General Public License for more details.
			You should have received a copy of the GNU Affero General Public License
			along with this program. If not, see <http://www.gnu.org/licenses/>.
 \brief		Distinct flows match Test class implementation.
 */

//Utility libs
#include "../../abycore/ENCRYPTO_utils/crypto/crypto.h"
#include "../../abycore/ENCRYPTO_utils/parse_options.h"
//ABY Party class
#include "../../abycore/aby/abyparty.h"

#include "common/distinct_match.h"

int32_t read_test_options(int32_t* argcp, char*** argvp,
		e_role* role, e_mode* mode, uint32_t* bytelen, bool* outsourced,
		uint32_t* num_rules_s, e_role* out_party, e_sharing* sharing,
		uint32_t* secparam, string* address, uint16_t* port) {

	uint32_t int_role = 9, int_mode = 9,
			int_out_party = 2, int_sharing = 0, int_port = 0;
	bool useffc = false;

	parsing_ctx options[] =
			{ {
				(void*) &int_role, T_NUM, "r",
					"Role: 0 = SERVER, 1 = CLIENT",
					true, false }, {
				(void*) &int_mode, T_NUM, "m",
					"Mode, 0 = MXOR, 1 = CLEAR, 2 = COLLAPSED",
					true, false }, {
				(void*) bytelen, T_NUM, "l",
					"Length of the rules (in bytes)",
					true, false }, {
				(void*) num_rules_s, T_NUM, "n",
					"Number of rules for server, default: 1",
					false, false }, {
				(void*) &int_out_party, T_NUM, "o",
					"Party receiving the output, 0 = SERVER, 1 = CLIENT, 2 = ALL, default: ALL",
					false, false }, {
				(void*) outsourced, T_FLAG, "x",
					"Use outsourced inputs, default: false",
					false, false }, {
				(void*) &int_sharing, T_NUM, "c",
					"Sharing algorithm, 0 = BOOL, 1 = YAO, default: BOOL",
					false, false }, {
				(void*) secparam, T_NUM, "s",
					"Symmetric Security Bits, default: 128",
					false, false }, {
				(void*) address, T_STR, "a",
					"IP-address, default: localhost",
					false, false }, {
				(void*) &int_port, T_NUM, "p",
					"Port, default: 7766",
					false, false }
			};

	if (!parse_options(argcp, argvp, options,
			sizeof(options) / sizeof(parsing_ctx))) {
		print_usage(*argvp[0], options, sizeof(options) / sizeof(parsing_ctx));
		cout << "Exiting" << endl;
		exit(0);
	}

	assert(int_role < 2);
	*role = (e_role) int_role;

	assert(int_mode < 3);
	*mode = (e_mode) int_mode;

	assert(int_out_party < 3);
	*out_party = (e_role) int_out_party;

	assert(int_sharing < S_LAST);
	*sharing = (e_sharing) int_sharing;

	if (int_port != 0) {
		assert(int_port < 1 << (sizeof(uint16_t) * 8));
		*port = (uint16_t) int_port;
	}

	//delete options;

	return 1;
}

int main(int argc, char** argv) {

	e_role role, out_party;
	e_mode mode;
	e_sharing sharing;
	bool outsourced = false;
	uint32_t bytelen = 40, num_rules_s = 1;

	uint32_t secparam = 128, nthreads = 1;
	uint16_t port = 7766;
	string address = "127.0.0.1";
	e_mt_gen_alg mt_alg = MT_OT;

	read_test_options(&argc, &argv,
			&role, &mode, &bytelen, &outsourced,
			&num_rules_s, &out_party, &sharing,
			&secparam, &address, &port);

	seclvl seclvl = get_sec_lvl(secparam);

	test_distinct_match_circuit(role, mode, bytelen, outsourced,
			num_rules_s, out_party, sharing,
			seclvl, (char*) address.c_str(), port,
			nthreads, mt_alg);

	return 0;
}
