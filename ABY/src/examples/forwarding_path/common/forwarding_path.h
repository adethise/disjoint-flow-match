/**
 \file 		forwarding_path.h
 \author 	Arnaud Dethise <arnaud.dethise@kaust.edu.sa>
 \copyright	ABY - A Framework for Efficient Mixed-protocol Secure Two-party Computation
			Copyright (C) 2015 Engineering Cryptographic Protocols Group, TU Darmstadt
			This program is free software: you can redistribute it and/or modify
			it under the terms of the GNU Affero General Public License as published
			by the Free Software Foundation, either version 3 of the License, or
			(at your option) any later version.
			This program is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
			GNU Affero General Public License for more details.
			You should have received a copy of the GNU Affero General Public License
			along with this program. If not, see <http://www.gnu.org/licenses/>.
 \brief		Implementation of the forwarding path detection
 */

#ifndef __FORWARDING_PATH_H_
#define __FORWARDING_PATH_H_

#include "../../../abycore/circuit/booleancircuits.h"
#include "../../../abycore/circuit/circuit.h"
#include "../../../abycore/aby/abyparty.h"

#include "../../distinct_match/common/distinct_match.h"

#include <cassert>

enum e_mode_fwd {
	UNSHUFFLED, SHUFFLED
};

/**
 * \brief	Create shares for each path and returns a pointer to an array
 * 		containing those that are selected, or the default no_match
 * 		value otherwise. The i-th path is selected if the i-th bit wire
 * 		in the selectors is true.
 * \param	circ		Boolean circuit to build upon
 * \param	fwd_paths	Pointer to an array of path identifiers. Only
 *				used for the server, values are ignored for
 *				the client (must still be instanciated).
 * \param	selectors	A share where each bit wire is a path selector.
 *				Must contain at least npaths bits
 * \param	npaths		Number of paths in fwd_paths
 * \param	no_match	Default value returned when a selector is false
 */
share** BuildSelectCircuit(BooleanCircuit *circ, e_role role,
		uint16_t *paths, share *selectors, uint32_t npaths,
		share *no_match, bool optimized_inputs = false);

/**
 * \brief	Returns an array containing the shares of paths, shuffled
 *		randomly (this assume srand was used outside of the function).
 *		The array will always have a size power of 2. The size is
 *		returned in the address pointed by size, and additional entries
 *		are padded with the no_match share.
 * \param	circ		Boolean circuit to build upon
 * \param	paths		Pointer to an array of path shares
 * \param	npaths		Size of the paths array
 * \param	no_match	Default share used for padding
 * \param	size		Address where to store the grid size
 */
share** BuildShuffleGrid(
		BooleanCircuit *circ,
		share **paths, uint32_t npaths, share *no_match,
		uint32_t *size,
		bool fakeseed = false);

/**
 * \brief	This function is used for running a testing environment for
 		solving the forwarding path sharing problem.
 * \param	role		Role of the executer in the SMPC computation
 * \param	address		IP Address of the SMPC computation server
 * \param	seclvl		Security level
 * \param	num_rules_s	Number of rules held by the server
 * \param	bytelen		Byte length of the match rule being tested
 * \param	nthreads	Number of threads used for the computation
 * \param	mt_alg		Multiplication triples generation algorithm
 */
int32_t test_forwarding_path_circuit(
		e_role role, e_mode_fwd mode, uint32_t bytelen, bool outsourced,
		uint32_t num_rules_s, e_role out_party, e_sharing sharing,
		seclvl seclvl, char* address, uint16_t port,
		uint32_t nthreads, e_mt_gen_alg mt_alg);
#endif /* __FORWARDING_PATH_H_ */
