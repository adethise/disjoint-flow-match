/**
 \file 		forwarding_path.cpp
 \author 	Arnaud Dethise <arnaud.dethise@kaust.edu.sa>
 \copyright	ABY - A Framework for Efficient Mixed-protocol Secure Two-party Computation
			Copyright (C) 2015 Engineering Cryptographic Protocols Group, TU Darmstadt
			This program is free software: you can redistribute it and/or modify
			it under the terms of the GNU Affero General Public License as published
			by the Free Software Foundation, either version 3 of the License, or
			(at your option) any later version.
			This program is distributed in the hope that it will be useful,
			but WITHOUT ANY WARRANTY; without even the implied warranty of
			MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
			GNU Affero General Public License for more details.
			You should have received a copy of the GNU Affero General Public License
			along with this program. If not, see <http://www.gnu.org/licenses/>.
 \brief		Implementation of the forwarding path sharing problem.
 */

#include "forwarding_path.h"

#define PATH_NO_MATCH 0xFFFFFFFF
#define FORWARDING_PATH_BITLEN 32

#define FORWARDING_PATH_OUT 1
#define FORWARDING_PATH_DBG_OUT 0
#define FORWARDING_PATH_DBG_OUT_COARSE 0
#define FORWARDING_PATH_DBG_MUX 0
#define FORWARDING_PATH_DBG_SHUFFLE 0
#define FORWARDING_PATH_DBG_SHUFFLE_KEY 0


share** BuildSelectCircuit(BooleanCircuit *circ, e_role role,
		uint64_t *paths, share *selectors, uint32_t npaths,
		share *no_match, bool optimized_inputs)
{
	assert(selectors->get_bitlength() >= npaths);
#if FORWARDING_PATH_DBG_OUT
	cout << ">> Building the select circuit" << endl;
	cout << "NO_MATCH constant: " << PATH_NO_MATCH << endl;
#endif

	// array for the returned values
	share **values = (share**) calloc(npaths, sizeof(share*));

	share* input;
	for (uint32_t i = 0 ; i < npaths ; i++) {
		if (optimized_inputs) {
			if (role == SERVER) {
				input = circ->PutSharedINGate(paths[i],
						FORWARDING_PATH_BITLEN);
			} else {
				input = circ->PutSharedINGate(paths[0],
						FORWARDING_PATH_BITLEN);
			}
		} else {
			if (role == SERVER) {
				input = circ->PutINGate(paths[i],
						FORWARDING_PATH_BITLEN, SERVER);
			} else {
				input = circ->PutINGate(paths[0],
						FORWARDING_PATH_BITLEN, SERVER);
			}
		}
		input = circ->PutCombinerGate(input);

		values[i] = circ->PutMUXGate(
				no_match,
				input,
				circ->PutRepeaterGate(FORWARDING_PATH_BITLEN,
					selectors->get_wire_ids_as_share(i))
				);
	}

	return values;
}

void shuffleSubGrid(
		BooleanCircuit *circ,
		share **paths, uint32_t n,
		share *key, uint32_t *k_idx)
{
	// this will put a conditional permutation for every entry in paths with
	// the entry placed at a distance n/2 of it
	assert(n % 2 == 0);

	share *permuted[2];

	for (uint32_t i = 0 ; i < n / 2 ; i++) {
		share *k_bit = key->get_wire_ids_as_share((*k_idx)++);
		permuted[0] = circ->PutMUXGate(paths[i], paths[i+n/2], k_bit);
		permuted[1] = circ->PutMUXGate(paths[i+n/2], paths[i], k_bit);

		paths[i] = permuted[0];
		paths[i+n/2] = permuted[1];
	}
}

share** BuildShuffleGrid(
		BooleanCircuit *circ,
		share **paths, uint32_t npaths, share *no_match,
		uint32_t *size,
		bool fakeseed)
{
	// for shuffling we need the size of the grid to be a power of 2
	uint32_t grid_size = 1 << ceil_log2_real(npaths);
	*size = grid_size;

	// path extraneous entries with no_match share
	share **grid = (share**) calloc(grid_size, sizeof(share*));
	for (uint32_t i = 0 ; i < grid_size ; i++) {
		if (i < npaths)
			grid[i] = paths[i];
		else
			grid[i] = no_match;
	}

	// generate n log n random bits to use as the shuffle key
	// each party will generate a random value, and they will be XORed to
	// decide the actual key, unknown to both parties
	uint32_t rbitlen, rbytelen;
	rbitlen = grid_size / 2 * ceil_log2_min1(grid_size);
	rbytelen = (rbitlen + 31) / 32;

	// We need n log n random bits for the shuffling.
	uint32_t *rvalue = (uint32_t*) calloc(rbytelen, sizeof(uint32_t));
	for (uint32_t i = 0 ; i < rbytelen ; i++) {
		rvalue[i] = rand();
	}

	share *key;
	if (fakeseed) {
		// This branch disable shared inputs for benchmark only (!)
		// compatibility with Yao
		key = circ->PutRepeaterGate(FORWARDING_PATH_BITLEN,
				circ->PutINGate(rvalue, rbitlen, SERVER));
	} else {
		key = circ->PutRepeaterGate(FORWARDING_PATH_BITLEN,
				circ->PutSharedINGate(rvalue, rbitlen));
	}
	uint32_t k_idx = 0; // index of the next unused random bit

#if FORWARDING_PATH_DBG_OUT
	cout << ">> Building the shuffle grid" << endl;
	cout << "Grid size: " << grid_size << endl;
	cout << "Shuffle key bitlength: " << rbitlen << endl;
#endif

	// at each iteration, every entry will be conditionally permuted with
	// the entry located at distance i/2 of it. This means that each entry
	// has an equal chance to end anywhere else depending on the key
	for (uint32_t i = 2 ; i <= grid_size ; i *= 2) {
		for (uint32_t j = 0 ; j < grid_size ; j += i) {
			// shuffle the subgrid of size i starting at index j
			// every bit > k_idx in key is unused
			shuffleSubGrid(circ, &grid[j], i, key, &k_idx);
		}
	}

#if FORWARDING_PATH_DBG_SHUFFLE_KEY
	// allows to print the key, but leaks information !
	circ->PutPrintValueGate(key, "Shuffle key");
#endif
#if FORWARDING_PATH_DBG_OUT
	cout << "Grid complete, number of permutations: " << k_idx << endl;
#endif

	free(rvalue);
	return grid;
}

/**
 * Run a test for the circuit reading from standard input.
 */
int32_t test_forwarding_path_circuit(
		e_role role, e_mode_fwd mode, uint32_t bytelen, bool outsourced,
		uint32_t num_rules_s, e_role out_party, e_sharing sharing,
		seclvl seclvl, char* address, uint16_t port,
		uint32_t nthreads, e_mt_gen_alg mt_alg)
{

	uint32_t bitlen = bytelen * 8;
	uint32_t maxgates = 20 * (bitlen + num_rules_s * num_rules_s);

	srand(time(NULL) + (int) role);

	// Read input from cin
	// n is the number of rules to read
	// p, m are the memory areas containing the rules
	// ptrns, masks are the arrays containing pointers to the rules
	uint32_t n;
	if (outsourced) {
		n = num_rules_s + 1;
	} else {
		n = (role == SERVER) ? num_rules_s : 1;
	}

	uint32_t nvals = num_rules_s;
#if FORWARDING_PATH_DBG_OUT
	cout << ">> Reading inputs from stdin" << endl;
	cout << "Expecting " << n << " rules" << endl;
#endif

	uint8_t *p, *m;
	uint8_t *ptrns[n], *masks[n];
	uint64_t paths[n]; // contains nextSDX, nextDeflector
	p = (uint8_t*) calloc(n, bytelen * sizeof(uint8_t));
	m = (uint8_t*) calloc(n, bytelen * sizeof(uint8_t));

	string input_line;
	for (uint32_t i = 0 ; i < n ; i++) {
		ptrns[i] = p + i * bytelen;
		getline(cin, input_line);
		parse_hexline(input_line, ptrns[i], bytelen);

		masks[i] = m + i * bytelen;
		getline(cin, input_line);
		parse_hexline(input_line, masks[i], bytelen);

		if (!outsourced && role == SERVER) {
			paths[i] = 0;
			uint64_t nextval;
			for (uint32_t j = 0 ; j < 2 ; j++) {
				getline(cin, input_line);
				nextval = (uint16_t) atoi(input_line.c_str());
				paths[i] |= nextval << (j * 16);
			}
		} else if (outsourced && i > 0) {
			paths[i-1] = 0;
			uint64_t nextval;
			for (uint32_t j = 0 ; j < 2 ; j++) {
				getline(cin, input_line);
				nextval = (uint16_t) atoi(input_line.c_str());
				paths[i-1] |= nextval << (j * 16);
			}
		} else {
			paths[i] = 0;
		}
	}

#if FORWARDING_PATH_DBG_OUT_COARSE
	for (uint32_t i = 0 ; i < n ; i++) {
		print_hexline("Pattern: ", ptrns[i], bytelen);
		print_hexline("Bitmask: ", masks[i], bytelen);
		if (role == SERVER)
			cout << "Forwarding: " << paths[i] << endl;
	}
#endif

	// ABY setup
#if FORWARDING_PATH_DBG_OUT
	cout << ">> Setting up the ABY party" << endl;
#endif
	ABYParty* party = new ABYParty(role, address, port,
			seclvl, bitlen, nthreads,
			mt_alg, maxgates);

#if FORWARDING_PATH_DBG_OUT
	cout << ">> Building the circuit" << endl;
#endif
	vector<Sharing*>& sharings = party->GetSharings();
	BooleanCircuit* circ =
		(BooleanCircuit*) sharings[sharing]->GetCircuitBuildRoutine();


	// Shares containing the problem information
	// arrays p1 p2 m1 m2 contain the patterns and matches of each party
	// arrays compare and result contain intermediate values
	// array out contains the reconstructed values
	share *s_p1, *s_m1, *s_p2, *s_m2;
	share *s_clear_result;


	// Put actual gates for ourself, dummy gates for the other actor
	// If sharing == S_YAO, we can not use outsourced (causes segfault)
	if (sharing == S_BOOL) {
		add_input_gates(&s_p1, &s_m1, &s_p2, &s_m2,
				role, circ, p, m, nvals, bitlen, outsourced,
				true);
	} else {
		add_input_gates(&s_p1, &s_m1, &s_p2, &s_m2,
				role, circ, p, m, nvals, bitlen, outsourced,
				false);
	}

	// Use the CLEAR mode of distinct_match to know which forwarding paths
	// overlaps with the client rule
	// 0 means the paths overlaps (is not disjoint)
	s_clear_result = BuildCLEARCircuit(circ, s_p1, s_m1, s_p2, s_m2);

	// Filter the entries to only keep those that have an overlap with the
	// client rule, replace the rest with a constant, fixed value
	share *no_match = circ->PutCombinerGate(
			circ->PutCONSGate(PATH_NO_MATCH,
				FORWARDING_PATH_BITLEN));

	share **s_filtered;
	if (sharing == S_BOOL) {
		s_filtered = BuildSelectCircuit(
				circ, role,
				paths, circ->PutSplitterGate(s_clear_result), nvals,
				no_match, true);
	} else {
		s_filtered = BuildSelectCircuit(
				circ, role,
				paths, circ->PutSplitterGate(s_clear_result), nvals,
				no_match, false);
	}

#if FORWARDING_PATH_DBG_MUX
	for (uint32_t i = 0 ; i < nvals ; i++) {
		circ->PutPrintValueGate(s_filtered[i], "filtered grid");
	}
#endif

	// Since revealing which rules have been matched leaks confidential
	// information from the client to the server, we shuffle the rules. This
	// way, only the number of rules matched and their associated forwarding
	// path next-hop is revealed.
	uint32_t grid_size;
	share **s_shuffled;
	if (mode == SHUFFLED) {
		if (sharing == S_YAO) {
			s_shuffled = BuildShuffleGrid(
					circ,
					s_filtered, nvals, no_match,
					&grid_size, true);
		} else {
			s_shuffled = BuildShuffleGrid(
					circ,
					s_filtered, nvals, no_match,
					&grid_size);
		}
	} else {
		s_shuffled = s_filtered;
		grid_size = nvals;
	}

#if FORWARDING_PATH_DBG_SHUFFLE
	for (uint32_t i = 0 ; i < grid_size ; i++) {
		circ->PutPrintValueGate(s_shuffled[i], "shuffled grid");
	}
#endif

	// Reconstruct the output of the grid
	share **s_out = (share**) calloc(grid_size, sizeof(share*));
	for (uint32_t i = 0 ; i < grid_size ; i++) {
		s_out[i] = circ->PutOUTGate(
				circ->PutSplitterGate(s_shuffled[i]),
				out_party);
	}

#if FORWARDING_PATH_DBG_OUT
	cout << ">> Built the circuit" << endl;
	cout << "Simulteous comparison of " << nvals << " rules" << endl;
	cout << "Shuffling of " << grid_size << " forwarding paths" << endl;
#endif

	party->ExecCircuit();

#if FORWARDING_PATH_DBG_OUT
	cout << ">> Computation complete, reading output" << endl;
#endif

	if (out_party == ALL || out_party == role) {
		// Read the output
		uint32_t out_bitlen, out_nvals;
		out_bitlen = s_out[0]->get_bitlength();
		out_nvals = s_out[0]->get_nvals_on_wire(0);

#if FORWARDING_PATH_DBG_OUT
		cout << "=PARAMS=============" << endl;
		cout << "Bitlen: " << out_bitlen << endl;
		cout << "Nvals: " << out_nvals << endl;
		cout << "Grid size: " << grid_size << endl;
		cout << "=OUTPUT=============" << endl;
#endif
#if FORWARDING_PATH_OUT
		for (uint32_t i = 0 ; i < grid_size ; i++) {
			uint64_t path = s_out[i]->get_clear_value<uint64_t>();
			if (path != PATH_NO_MATCH) {
				// Only print useful information
				cout << (path & 0xFFFF) << ":";
				cout << ((path >> 16) & 0xFFFF) << endl;
			}
		}
#endif
	}

#if FORWARDING_PATH_DBG_OUT
	cout << ">> Cleaning up" << endl;
#endif

	free(p);
	free(m);

	free(s_filtered);
	if (mode == SHUFFLED)
		free(s_shuffled);
	free(s_out);
	delete party;

	return 0;
}
