#!/usr/bin/env python3

import pickle
import re
import sys
from collections import defaultdict
from statistics import mean, stdev

PATTERNS = {
        'Setup time': re.compile(r'Setup =\s+(?P<val>\d+(\.\d+)?) ms'),
        'Online time': re.compile(r'Online =\s+(?P<val>\d+(\.\d+)?) ms'),
        'Communication time': re.compile(r'Communication: (?P<val>\d+(\.\d+)?)'),
        'Bytes tranferred (setup)': re.compile(r'Setup Sent / Rcv\s+(?P<val>\d+)  bytes / \d+ bytes'),
        'Bytes tranferred (online)': re.compile(r'Online Sent / Rcv\s+(?P<val>\d+)  bytes / \d+ bytes'),
}


def _make_key(entry):
    return (entry['circuit'], entry['mode'], entry['delay'], entry['length'], entry['num_rules'])


def extract_value(entry, pattern):
    client_value = float(pattern.search(entry['client_output']).group(1))
    server_value = float(pattern.search(entry['server_output']).group(1))

    return max(client_value, server_value)


def make_groups(entries):
    groups = defaultdict(list)

    for entry in entries:
        groups[_make_key(entry)].append(entry)

    return groups


def compute_all_tables(groups):
    '''
    In res, the first key correspond to a table (e.g. online time) and the
    second key correspond to a cell of this table (for a given delay, length,
    num_rules and mode).
    The value is a tuple (mean, variance).
    '''
    res = {}

    for table in PATTERNS:
        res[table] = {}
        pattern = PATTERNS[table]

        for cell in groups:
            entries = groups[cell]
            values = [extract_value(entry, pattern) for entry in entries]

            mean_value = mean(values)
            stdev_value = stdev(values, mean_value)

            res[table][cell] = (mean_value, stdev_value)

    return res


def _pretty_float(f):
    return ('%s' % float('%.4g' % f)).rstrip('0').rstrip('.')


def print_formatted(results):
    tables = results.keys()

    for table in tables:
        print(table, ':')
        print('\tcircuit\tmode\tdelay\tlength\trules\t|  mean\t\tstdev')
        print(6 * ' ' + 66 * '-')

        cells = results[table]

        for cell in sorted(cells, key = lambda x: tuple(map(int, x))):
            c, m, d, l, n = cell
            mean, var = map(_pretty_float, results[table][cell])

            print('\t{:<8}{:<8}{:<8}{:<8}{:<8}'.format(c, m, d, l, n) +
                    '|  ' + '{:<13}{:<12}'.format(mean, var))
        print()




if __name__ == '__main__':
    INPUT_FILE = sys.argv[1]

    data    = pickle.load(open(INPUT_FILE, 'rb'))
    groups  = make_groups(data)
    results = compute_all_tables(groups)
    print_formatted(results)
