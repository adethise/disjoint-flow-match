# Disjoint flow match detection

## Running with Vagrant

    vagrant up --provision
    vagrant ssh
    cd distinct_match/
    ./launch.sh

## Running manually

- Install aby dependencies

    g++ make libgmp-dev libglib2.0-dev libssl-dev


- Clone the ABY repo

    git clone --recursive https://github.com/encryptogroup/ABY.git

- Copy the disjoint flow match files to the ABY tree

    cp -rv distinct_match ABY/src/examples/

- Run with tmux with `launch.sh` or cd in the ABY directory and run `make`
