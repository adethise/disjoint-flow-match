#!/usr/bin/env python3

'''
Script file for generating test data with the SMPC disjoint flow matching
program.
This script will run the executable and collect the output, then dump it with
pickle to a file. Said file can then be used at any time to compute statistics
on the runs.

author: Arnaud Dethise <arnaud.dethise@kaust.edu.sa>
date: 2017-03-27
'''

import sys
import pickle
from datetime import datetime
from subprocess import Popen, PIPE, DEVNULL, TimeoutExpired


#######################
# Tests configuration #
#######################

EXEC = './ABY/bin/distinct_match.exe'

INPUT_DIR = 'ABY/src/examples/distinct_match/inputs/'
SERVER_INPUT = INPUT_DIR + 'server-l54-r10000'
CLIENT_INPUT = INPUT_DIR + 'l54-2'

OUTPUT_FILE = ('test-out-data-dm-%s.dat' %
        datetime.now().strftime('%Y-%m-%d-%H:%M'))

# Tuples of (length, num_rules) to use for testing
TESTS = [
        ('13', '1'),
        ('13', '25'),
        ('13', '50'),
        ('13', '100'),
        ('13', '250'),
        ('13', '500'),
        ('13', '2000'),
        ('13', '5000'),
        ('13', '10000'),
        ('37', '1'),
        ('37', '25'),
        ('37', '50'),
        ('37', '100'),
        ('37', '250'),
        ('37', '500'),
        ('37', '2000'),
        ('37', '5000'),
        ('37', '10000'),
        ('54', '1'),
        ('54', '25'),
        ('54', '50'),
        ('54', '100'),
        ('54', '250'),
        ('54', '500'),
        ('54', '2000'),
        ('54', '5000'),
        ('54', '10000'),
]

# Possible run modes
MODES = {'0': 'MXOR', '1': 'CLEAR', '2': 'COLLAPSED'}
CIRCUITS = {'0': 'BOOL', '1': 'YAO'}

# Communication delays to use
DELAYS = ['0', '10', '50', '100', '300']

# Number of time to run tests for each combination
REPEAT = 50

errors = 0 # global variable



def test(length, num_rules, mode, circuit, timeout):
    '''
    Run a single test and return the output.
    Params:
        length    - length of the rule pattern
        num_rules - number of rules to read and test for the server
        mode      - run mode
        circuit   - circuit to use (bool or yao)
    '''

    server = Popen(
            [EXEC,
                '-r', '0',
                '-l', length,
                '-n', num_rules,
                '-m', mode,
                '-c', circuit],
            stdin = open(SERVER_INPUT),
            stdout = PIPE,
            universal_newlines = True
    )
    client = Popen(
            [EXEC,
                '-r', '1',
                '-l', length,
                '-n', num_rules,
                '-m', mode,
                '-c', circuit],
            stdin = open(CLIENT_INPUT),
            stdout = PIPE,
            universal_newlines = True
    )

    try:
        server.wait(timeout = timeout)
        client.wait(timeout = 1)
    except TimeoutExpired:
        print(('Execution failed for l={}, n={}, mode={}, circuit={}, ' +
            'timeout={}').format(length, num_rules, mode, circuit, timeout))
        global errors
        errors += 1
        return None

    res = {}
    res['length'] = length
    res['mode'] = mode
    res['num_rules'] = num_rules
    res['circuit'] = circuit
    res['server_output'] = server.stdout.read()
    res['client_output'] = client.stdout.read()

    return res


def set_delay(delay):
    '''
    Configure the localhost transfer delay with tc-netem.
    Requires sudo privileges.
    Params:
        delay - the new delay to set
    '''
    # Remove previous rule
    Popen('sudo tc qdisc del dev lo root'.split(), stderr = DEVNULL).wait()
    # Add new rule
    Popen(('sudo tc qdisc add dev lo root' +
            ' handle 1:0 netem delay {}msec'.format(delay)).split()).wait()


def test_with_delay(delay, ntests):
    '''
    Run all configured tests (length, num_rules, mode) for a single delay.
    Resets the delay after execution.
    Warning: interrupting the program during this function will leave the
    tc-netem localhost delay rule in place.
    Params:
        delay  - the communication delay to simulate
        ntests - the number of times the tests must be run
    '''

    set_delay(delay)
    timeout = 60 + int(delay) / 5 # timeout in seconds

    res = []
    for length, nrules in TESTS:
        print('Testing rule {}:{} with delay {}'.format(length, nrules, delay))
        for mode in MODES:
            for circuit in CIRCUITS:
                print('\t- mode %s, circuit %s' %
                        (MODES[mode], CIRCUITS[circuit]))
                for _ in range(REPEAT):
                    test_result = test(length, nrules, mode, circuit, timeout)
                    if test_result != None:
                        test_result['delay'] = delay
                        res.append(test_result)

    set_delay(0)
    return res




if __name__ == '__main__':
    print('Generating test data for the distinct match problem')
    print('%s tests to run, repeated %s times for %s modes and %s circuits' %
            (len(TESTS) * len(DELAYS), REPEAT, len(MODES), len(CIRCUITS)))

    print('Results will be written to %s' % OUTPUT_FILE)

    results = []
    for delay in DELAYS:
        results += test_with_delay(delay, REPEAT)

    print('Tests completed. %s results, %s errors' % (len(results), errors))

    with open(OUTPUT_FILE, 'wb') as out:
        pickle.dump(results, out)
