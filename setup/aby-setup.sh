#!/usr/bin/env bash

sudo apt-get install -y g++ make libgmp-dev libglib2.0-dev libssl-dev

cd ~/distinct_match/
git clone --recursive https://github.com/encryptogroup/ABY.git

cp -rv /home/vagrant/distinct_match/distinct_match /home/vagrant/distinct_match/ABY/src/examples/distinct_match
