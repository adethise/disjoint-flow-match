#!/usr/bin/env bash

sudo apt-get update

sudo apt-get install -y build-essential vim ssh git sshfs tmux
